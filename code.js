/*  1. Єкранування це використання специальних символів як звичайних за рахунок
    інших символів.
    Наприклад: 
    зворотна коса риса \ використовується для позначення класів символів, 
    наприклад, \d. Це спеціальний символ у регулярних виразах (як і в звичайних 
    рядках).
    Є й інші спеціальні символи, які мають особливе значення у регулярному
    виразі. Вони використовуються для складніших пошукових конструкцій. Ось 
    повний список цих знаків: [ ] \ ^ $ . | ? * + ( ).
    Не треба намагатися запам'ятати цей список: ми розберемося з кожним з них
    окремо, і таким чином ви вивчите їх «автоматично».

    2.Виклик функції. Оголошена функція сама собою не виконується. Запуск функції
    виконується за допомогою її виклику.

    При цьому коли ми оголошуємо функцію з ім'ям, ми тим самим створюємо нову 
    змінну з цією назвою. Ця змінна буде функцією. Для виклику функції необхідно 
    вказати її ім'я та дві круглі дужки, у яких за необхідності можна передати 
    аргументи. Відокремлення одного аргументу від іншого виконується за допомогою 
    коми.

    3.Hoisting, тобто спливання, підняття, це механізм, при якому змінні та 
    оголошення функції піднімаються вгору по своїй області видимості перед
    виконанням коду. Однією з переваг підйому є те, що він дозволяє нам 
    використовувати функції перед їх оголошенням у коді.*/


const firstName = prompt("What is your name"); 
const lastName = prompt("Your surname");
const birthday = prompt("Enter your date of birth in the format DD.MM.YYYY");

 function creatNewUser(firstName, lastName, birthday) {
 return {
    firstName: firstName,
    lastName: lastName,
    birthday: birthday,
    
    getLogin(){
        return((this.firstName[0]).toUpperCase() 
        + (this.lastName).toLowerCase());
    },
        getAge(){
        const [day, month, year] = birthday.split(".");
        const date = new Date();
        date.setFullYear(year);
        date.getMonth(month - 1);
        date.getDay(day);
        const now = new Date();
        const secondsInYear = 60 * 60 * 24 * 365;
        const millesecondsInSecond = 1000;
        return Math.floor((now - date) / millesecondsInSecond / secondsInYear);
    },
    getPassword(){
        return((this.firstName[0]).toUpperCase() 
        + (this.lastName).toLowerCase() 
        + (this.birthday).slice(6));
    },

};
}; 
const newUser = creatNewUser(firstName, lastName, birthday);

console.log(creatNewUser(firstName, lastName, birthday));
console.log(newUser.getPassword());
console.log(newUser.getAge());